const express = require('express');
const router = express.Router();
const gigsController = require('../controllers/gigs');

router.get('/', gigsController.getAllGigs);

router.get('/add', gigsController.getAddGigForm);

router.post('/add', gigsController.getAddGig);

router.get('/search', gigsController.getSearchLogic);

module.exports = router;