const express = require("express");
const exphbs = require("express-handlebars");
const bodyParser = require("body-parser");
const path = require("path");
const dbConnection = require("./config/database.js");

const app = express();
const PORT = process.env.PORT || 3000;

// dbConnection.authenticate().then(() => {
//     console.log("Database Connection was successful!");
// }).catch((err) => {
//     console.log("Failed To Connect To Database: " + err);
// });

//handlebars config
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.urlencoded({extended: false}))

//serving static files
app.use(express.static(path.join(__dirname, 'public')));

//routes
 app.get('/', (req, res) => {
    res.render('index', { layout: 'landing' });
 });

app.use('/gigs', require("./routes/gigs"));

// app.use((req, res, next) => {
//     let error = new Error('404: Page Not Found');
//     error.statusCode = 404;
//     next(error);
// });

// app.use((error, req, res, next) => {
//     res.statusCode = req.statusCode || 500;
//     console.log('404');
//     // res.render("404");
// });


app.listen(PORT, () => {
    console.log(`Server Listening on Port ${PORT}`);
});
