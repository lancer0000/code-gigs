const db = require('../config/database');

exports.getAllGigs = async (req, res) => {
    try {
         const rows = await db.query('SELECT * FROM gigs');
         const gigsArray = [];
         rows.forEach(element => {
             gigsArray.push(JSON.parse(JSON.stringify(element)));
         });
         res.render('gigs', {gigs: gigsArray});
    } catch (e){
         console.log(e);
    }
};

exports.getAddGigForm = (req, res, next) => {
    res.render('add');
};

exports.getAddGig = async (req, res, next) => {
    const gig = {
        title: req.body.title,
        technologies: req.body.technologies,
        budget: req.body.budget,
        description: req.body.description,
        contactemail: req.body.contact_email
    };
    let errors = [];
    if (!gig.title) {
        errors.push({error: 'The field title is required'});
    }
    if (!gig.technologies) {
        errors.push({error: 'The field technologies is required'});
    }
    if (!gig.description) {
        errors.push({error: 'The field description is required'});
    }
    if (!gig.contactemail) {
        errors.push({error: 'The field contact email is required'});
    }

    if (errors.length > 0) {
        res.render('add', {errors, title: gig.title, technologies: gig.technologies,
             budget: gig.budget, description: gig.description, contact_email: gig.contactemail});
    } else {
        try {
            const gig = {
                title: req.body.title,
                technologies: req.body.technologies,
                budget: req.body.budget ? `$${req.body.budget}` : 'Unknown',
                description: req.body.description,
                contactemail: req.body.contact_email
            };
            const queryRes = await db.query(`INSERT INTO gigs (technologies, budget, description, title) VALUES 
            ('${gig.technologies}', '${gig.budget}', '${gig.description}',
                 '${gig.title}')`)
            res.redirect('/gigs');
        } catch (e) {
            console.log(e);
        }
    }
};

exports.getSearchLogic = async (req, res, next) => {
    const search = req.query.search.toLowerCase();
    try {
        const searchedGigsArray = [];
        const rows = await db.query(`SELECT * FROM gigs WHERE technologies LIKE '%${search}%'`);
        rows.forEach(element => {
            searchedGigsArray.push(JSON.parse(JSON.stringify(element)));
        });
        res.render('gigs', {gigs: searchedGigsArray, searchTerm: search});
    } catch(e) {
        console.log(e);
    }
};